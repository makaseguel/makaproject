include scripts/vars.mk
-include scripts/secrets.mk
## Deploy container
deploy:
	@scripts/build.sh

cloud-deploy:
	@scripts/cloud-docker-build.sh

## Check repositories list in gcloud
repositories:
	gcloud artifacts repositories list

deploy-dag:
	gsutil cp airflow/dag.py gs://us-west2-data-orquestator-0fa8409c-bucket/dags/${SERVICE}.py

# Installs libraries locally
install:
	pip install -r requirements.txt

# Update libraries in requirements.txt
update-libraries:
	pip freeze > requirements.txt

# Run job locally
run:
	python -m flask run --host=0.0.0.0 --port=8080

# Run tests locally
test:
	python tests/test_healthcheck.py