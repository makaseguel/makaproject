# utf-8
# [START eventarc_gcs_server]
import json
import os
import logging
from infraestructure.conf import Config
from flask import Flask, request

from usecases.db.handler import DbHandler
from utils.read_params import ReadParams
from usecases.healthcheck import urls as hcheck_urls
from usecases.gbq.handler import BigqueryHandler


app = Flask(__name__)

CONFIG = Config()
LOGGER = logging.getLogger('service-name')
DATE_FORMAT = """%(asctime)s,%(msecs)d %(levelname)-2s """
INFO_FORMAT = """[%(filename)s:%(lineno)d] %(message)s"""
LOG_FORMAT = DATE_FORMAT + INFO_FORMAT
logging.basicConfig(format=LOG_FORMAT, level=logging.INFO)


# Healthcheck Block
app.register_blueprint(hcheck_urls.healthcheck_blueprint(CONFIG, ReadParams(), LOGGER))
# end block



# Bigquery block #
@app.route('/gbq/get_data', methods=['POST', 'GET'])
def bigquery_get_data():
    PARAMS = ReadParams(request.json or {})
    return BigqueryHandler(CONFIG, PARAMS, LOGGER).bq_get_data()
# End Bigquery block #

# Db block #
@app.route('/db/save_data', methods=['POST'])
def save_data():
    PARAMS = ReadParams(request.json)
    LOGGER.info(request.json['body'])
    return DbHandler(CONFIG, PARAMS, LOGGER).save(data=json.loads(request.json['body']))
# End Db block #


# [START eventarc_gcs_server]
if __name__ == "__main__":
    app.run(debug=True, host='0.0.0.0', port=int(os.environ.get('PORT', 8080)))
# [END eventarc_gcs_server]
