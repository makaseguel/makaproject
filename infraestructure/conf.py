from os import environ
from typing import NamedTuple


# Logger tuple that contain all definitions for logging usage
class Logger(NamedTuple):
    LogLevel: str = environ.get("LOGGER_LOG_LEVEL", "gunicorn.error")


# Server tuple that contains all definitions for server init
class Server(NamedTuple):
    host: str = environ.get("SERVER_HOST", "0.0.0.0")
    port: int = int(environ.get("SERVER_PORT", 5000))

class Bigquery(NamedTuple):
    project_id: str = environ.get("BIGQUERY_PROJECT_ID", "data-poc-323413")
    dataset_id: str = environ.get("BIGQUERY_DATASET_ID", "analytics_279907210")
    table_id: str = environ.get("BIGQUERY_TABLE_ID", "events")

# Database tuple that contains all definitions for its conection
class Database(NamedTuple):
    host: str = environ.get("DATABASE_HOST", "0.0.0.0")
    port: int = int(environ.get("DATABASE_PORT", 5432))
    name: str = environ.get("DATABASE_NAME")
    user: str = environ.get("DATABASE_USER", "docker")
    password: str = environ.get("DATABASE_PASSWORD", "docker")


# Config type to contain all definitions of configs
class Config(NamedTuple):
    logger: Logger = Logger()
    server: Server = Server()
    database: Database = Database()
    bigquery: Bigquery = Bigquery()
