# pylint: disable=no-member
# utf-8

# import pandas_gbq
import pandas as pd


class BigQuery:
    """
    Class that allow doing operations with gbq.
    """
    def __init__(self, conf) -> None:
        self.conf = conf

    def query_to_dataframe(self, query) -> pd.DataFrame:
        """
        Method that from query transform raw data into data frame.
        """
        # self.logger.info('Query : %s', query.replace(
        #     '\n', ' ').replace('    ', ' '))
        data = pd.read_gbq(query, project_id=self.config.bigquery.project_id)
        data_frame = pd.DataFrame(data)
        return data_frame
