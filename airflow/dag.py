from __future__ import print_function

from datetime import datetime, timedelta
import logging
import requests
from airflow import models
from airflow.operators import bash_operator
from airflow.operators import python_operator

default_args = {
    # The start_date describes when a DAG is valid / can be run. Set this to a
    # fixed point in time rather than dynamically, since it is evaluated every
    # time a DAG is parsed. See:
    # https://airflow.apache.org/faq.html#what-s-the-deal-with-start-date
    'start_date': datetime(2022, 2, 12),
}

SERVICE_URL = 'https://py-makaproject-nasdocrtnq-ue.a.run.app'
# Define a DAG (directed acyclic graph) of tasks.
# Any task you create within the context manager is automatically added to the
# DAG object.


# Calculate the date to be used for the next execution of the DAG
def get_date(**kwargs):
    date = {}
    execution_date = kwargs['execution_date']
    if kwargs['dag_run'].external_trigger:
        execution_date = execution_date - timedelta(days=1)
    # execution_date to string
    execution_date = execution_date.date().strftime('%Y-%m-%d')
    if 'start_date' in kwargs['dag_run'].conf:
        date['start_date'] = kwargs['dag_run'].conf['start_date']
    else:
        date['start_date'] = execution_date
    if 'end_date' in kwargs['dag_run'].conf:
        date['end_date'] = kwargs['dag_run'].conf['end_date']
    else:
        date['end_date'] = execution_date
    return date


with models.DAG(
        'composer_makaproject',
        schedule_interval=None,
        default_args=default_args) as dag:
    # Define tasks. Each task can have multiple operators and the DAG

    def get_data_gbq(**kwargs):
        logging.info('Getting data from bq to DWH')
        data = requests.post(SERVICE_URL + "/gbq/get_data", json=get_date(**kwargs))
        return data.json()

    def save_data_dwh(**kwargs):
        logging.info('Saving data in DWH')
        ti = kwargs['ti']
        data = ti.xcom_pull(task_ids="get_data_from_gbq")
        data.update(get_date(**kwargs))
        logging.info(data)
        data = requests.post(SERVICE_URL + "/db/save_data", json=data)
        return data.json()


    get_data = python_operator.PythonOperator(
        task_id="get_data_from_gbq",
        provide_context=True,
        python_callable=get_data_gbq)

    save_data = python_operator.PythonOperator(
        task_id="save_data_in_DWH",
        provide_context=True,
        python_callable=save_data_dwh)

    end_bash = bash_operator.BashOperator(
        task_id='End',
        bash_command='echo Success.')

    get_data >> save_data >> end_bash
