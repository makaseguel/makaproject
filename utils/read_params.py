from datetime import datetime, timedelta


class ReadParams:
    """
    Class that allow read params from dag conf.
    Reads what comes from dag config, but if not config is detected
    would be setting (now() -1 day) by default
    """
    def __init__(self, params={}) -> None:
        self.start_date = params
        self.end_date = params

    @property
    def start_date(self):
        return self.__start_date
    
    @start_date.setter
    def start_date(self, date):
        if not "start_date" in date:
            self.__start_date = (datetime.now() - timedelta(days=1)).strftime("%Y-%m-%d")
        else:
            self.__start_date = date['start_date']
    
    @property
    def end_date(self):
        return self.__end_date
    
    @end_date.setter
    def end_date(self, date):
        if not'end_date' in date:
            self.__end_date = (datetime.now() - timedelta(days=1)).strftime("%Y-%m-%d")
        else:
            self.__end_date = date['end_date']
