
class Response:
    """
    Response:
    Create a fresh object to be used as response
    Ex.
    return Response().Success({'status': 'OK}, 'msg if needed')
    """
    def output(self, status, data, extra=False):
        response = {
            "statusCode": status,
            "body": data
        }
        if extra:
            response['extra'] = extra
        return response

    def Error(self, msg):
        msg = "Error - " + msg
        return self.output(400, msg)
    
    def Success(self, data, msg=False):
        return self.output(200, data, msg if msg else False)
