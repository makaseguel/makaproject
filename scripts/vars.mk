#!/usr/bin/env bash
export FLASK_APP=main
export FLASK_ENV=development
export REGION=us-west2
export PROJECT=$(gcloud config get-value project)

export SERVICE=template
export TAG=master
export CONTAINER=gcr.io/${PROJECT_ID}/$(SERVICE):$(TAG)
export PROJECT_NO=$(gcloud projects list --filter="${PROJECT}" --format="value(PROJECT_NUMBER)")
export SVC_ACCOUNT="${PROJECT_NO}-compute@developer.gserviceaccount.com"
