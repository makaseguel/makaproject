# Template repository to create pipelines
## Considerations before usage
It's important to name your new repo using SRE's standards, please be sure to read this doc before going foward, if you use this template your are accepting being acknowledge these standards.
```
https://yapo.atlassian.net/wiki/spaces/YAPO/pages/3094609950/Gitlab+Naming+Agreements
```

## Installment
### Requires python 3.7.0 - 3.9.x
```
check in terminal with 'python --version'
```
### Requires pip3

```
pip3 install virtualenv
git clone git@gitlab.com:yapo_team/yotf/data-analytics/pipelines/<scheme>/<pipeline>.git
virtualenv --python=python3.9 <virtualenv name> 

source <virtualenv name>/bin/activate
```

### First you need to install all needed libraries
```
make install
```

### if you installed new libraries, it is necessary to update the requirements.txt file
```
make update-libraries
```

### Start app locally (needs to run make install first because some libraries are a must)
```
make run
```
#### by default **http://0.0.0.0:8080/** would be available

### Run tests
```
make test
```

## Considerations
### It's important to change service name, so you may do that the way you please replacing every "template" word by the service name you may want to use
You may find this script useful to get it done
```
grep -rl "template" ../\<VIRTUAL ENV FOLDER>/* | xargs sed -i '' "s/template/\<SERVICE NAME>/g"
```

### Also remember to do update the README.md, be kind with your future self

## more info soon