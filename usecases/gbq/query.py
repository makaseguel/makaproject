# pylint: disable=no-member
# utf-8

from datetime import datetime, timedelta


class GetBigquery:
    def get_query(self, date):
        start_date = str(date)
        dobj = datetime.strptime(start_date, '%Y-%m-%d')
        d = dobj + timedelta(days=1)
        ingestion_date = d.strftime('%Y-%m-%d')
        project_id = self.config.bigquery.project_id
        dataset_id = self.config.bigquery.dataset_id
        table_id = self.config.bigquery.table_id
        bq_route = project_id + '.' + dataset_id + '.' + table_id + '_' + start_date.replace("-", "")

        return """
                  SELECT 
                    '%s' AS date,
                     COUNT(DISTINCT CONCAT(user_pseudo_id, session_id)) AS visits,
                     '%s' AS ingestion_date
                    FROM (
                     SELECT
                       user_pseudo_id,
                       (
                       SELECT
                         value.int_value
                       FROM
                         UNNEST(event_params)
                       WHERE
                         key = 'ga_session_id') AS session_id
                     FROM
                         `%s`)
  
  """ % (start_date, ingestion_date, bq_route)
