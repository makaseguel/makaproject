# pylint: disable=no-member
# utf-8

import json
import pandas as pd

from infraestructure.gbq import BigQuery
from utils.response import Response
from .query import GetBigquery


class BigqueryHandler():
    def __init__(self,
                 config,
                 params,
                 logger) -> None:
        self.config = config
        self.params = params
        self.logger = logger


    def bq_get_data(self):

        self.logger.info('Preparing Query to BigQuery with date %s' % self.params.start_date)
        data = BigQuery.query_to_dataframe(self, GetBigquery.get_query(self, self.params.start_date))
        self.logger.info('Response received')

        return Response().Success(data.to_json(orient='records'), 'OK')
