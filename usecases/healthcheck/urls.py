# pylint: disable=no-member
# utf-8
from flask import Blueprint
from .handler import Healthcheck


def healthcheck_blueprint(CONFIG, PARAMS, LOGGER):
    healthcheck_urls = Blueprint('healthcheck_urls', __name__, url_prefix='/healthcheck')

    # URL's LIST
    @healthcheck_urls.route('/status', methods=['GET'])
    def get_status():
        return Healthcheck(CONFIG, PARAMS, LOGGER).get_status()
    # End list

    return healthcheck_urls