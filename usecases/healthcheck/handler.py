# pylint: disable=no-member
# utf-8
from utils.response import Response


class Healthcheck():
    def __init__(self,
                 config,
                 params,
                 logger) -> None:
        self.config = config
        self.params = params
        self.logger = logger

    def get_status(self):
        self.logger.info('Accesing healthcheck')
        return Response().Success({'status': 'OK'}, 'success')

