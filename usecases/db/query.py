# pylint: disable=no-member
# utf-8

class DbQueries:
    def delete(self):
        return """delete from 
                {dataset}.{table} 
                where 
                    time_date::date 
                    between 
                    '{start_date}'::date 
                    and 
                    '{end_date}'::date""".format(start_date=self.params.start_date,
                                                 end_date=self.params.end_date,
                                                 dataset=self.dataset,
                                                 table=self.table)




