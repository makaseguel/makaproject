# pylint: disable=no-member
# utf-8
import pandas as pd
from utils.response import Response
from infraestructure.psql import Database
from .query import DbQueries


class DbHandler():
    def __init__(self,
                 config,
                 params,
                 logger) -> None:
        self.config = config
        self.params = params
        self.logger = logger
        self.dataset = 'testing'
        self.table = 'visits'

    def insert_to_table(self, dataset, table):
        dwh = Database(conf=self.config.database)
        dwh.insert_copy(self.data, dataset, table)

    def save(self, data):
        self.data = pd.DataFrame(data)
        self.insert_to_table(self.dataset, self.table)
        return Response().Success('Saved successfully')

    def delete(self):
        self.logger.info('Preparing to delete from DWH')
        dwh = Database(conf=self.config.database)
        self.logger.info("Cleaning {}.{}".format(self.dataset, self.table))
        dwh.execute_command(DbQueries.delete(self))
        return Response().Success('Delete from DWH done')



