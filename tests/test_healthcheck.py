# pylint: disable=no-member
# utf-8
import unittest

import sys
import os
current = os.path.dirname(os.path.realpath(__file__))
parent = os.path.dirname(current)
sys.path.append(parent)
# importing app
from main import app


class TestHealthcheck(unittest.TestCase):
    # executed prior to each test
    def setUp(self):
        app.config['TESTING'] = True
        app.config['WTF_CSRF_ENABLED'] = False
        app.config['DEBUG'] = False
        self.app = app.test_client()

        # Testing debug
        self.assertEqual(app.debug, False)
    
    def test_ok(self):
        response = self.app.get('/healthcheck/status', follow_redirects=True)
        self.assertEqual(response.status_code, 200)
        self.assertDictEqual(response.json, {"body": { "status": "OK" },
                                             "extra": "success",
                                             "statusCode": 200
                                            })
    
    def test_bad_method(self):
        response = self.app.post('/healthcheck/status', follow_redirects=True)
        self.assertEqual(response.status_code, 405)
    
    def test_not_found(self):
        response = self.app.get('/healthcheck', follow_redirects=True)
        self.assertEqual(response.status_code, 404)

if __name__ == "__main__":
    unittest.main()